Justrate::Application.routes.draw do

  get "reviews/new" => "reviews#new_blank"
  post "reviews/create_blank" => "reviews#create_blank"
  get "users/new"
  get "users/create"

  # Some static pages served with the StaticPage controller
  #get "static_page/about_us"
  get '/about_us' => 'static_page#about_us'
  get '/privacy' => 'static_page#privacy'
  get '/toc' => 'static_page#toc'

  # Set up routes for Users such as users/sign_up, users/sign_in
  devise_for :users, :controllers => {:confirmations => 'confirmations', :omniauth_callbacks => "omniauth_callbacks"}
  devise_scope :user do
    post "/confirm" => "confirmations#confirm"
  end

  # Set up routes for Admins
  devise_for :admins

  resources :users, :only => [:show, :edit, :update]
  resources :departments do
    get :course_list
  end
  resources :universities

  resources :courses do
    resources :reviews do
      member do
        post :vote_for
        post :vote_against
        post :flag
      end
      resources :comments, :shallow => true do
        get :reply
        post :new_reply
      end
    end
  end

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => 'courses#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
