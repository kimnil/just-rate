require 'factory_girl'

FactoryGirl.define do

  factory :course do
    sequence :name do |n|
      "Automatic generated test course #{n}"
    end
    
    sequence :course_number do |n|
      "TEST#{n}"
    end

    description 'Test Course description'
    lecturer 'Professor Test'

    association :department, factory: :department

    factory :invalid_course do
      name '' # skip name here to make in invalid
    end
  end 

end