require 'factory_girl'

FactoryGirl.define do

  factory :review do
    text 'generated review'
    title 'generated title'

    course
  end 

end
