require "spec_helper"

describe Course do

  it "should be able to create a new course" do
    # Shouldn't have any courses to begin with ... 
    # Note that the database is cleaned between specs
    Course.count.should be 0 

    course = FactoryGirl.build(:course)
    course.save!

    # ... but after saving a valid course it should be 1.
    Course.count.should be 1
  end

  it "should not be possible to create a course without a name" do
    invalid_course = FactoryGirl.build(:invalid_course)

    # Save an invalid course and make sure an error is raised.
    -> {
        invalid_course.save!
    }.should raise_error
  end

  it "should be possible to get the average rating even though no one has rated" do
    course = FactoryGirl.build(:course)
    course.save!

    course.average_score.should be 0
  end

end