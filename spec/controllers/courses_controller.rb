require 'spec_helper.rb'

describe CoursesController do

  include Devise::TestHelpers

  describe "GET #index" do
    it "responds successfully with an HTTP 200 status code" do
      get :index
      expect(response).to be_success
      expect(response.status).to eq(200)
    end

    it "renders the index template" do
      get :index
      expect(response).to render_template("index")
    end

    it "loads all of the courses into @courses" do
      # generate a few courses and store them for later matching
      courses = []
      5.times do 
        courses << FactoryGirl.create(:course)
      end

      get :index

      expect(assigns(:courses)).to match_array(courses)
    end
  end

  describe 'DELETE destroy' do 
    before :each do 
      @course = FactoryGirl.create(:course) 
    end 

    it "prevents deletion of courses if not logged in" do 
      delete :destroy, id: @course.id
      response.status.should be 302
    end 
  end
end