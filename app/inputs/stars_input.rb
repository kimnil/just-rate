class StarsInput < SimpleForm::Inputs::Base
  def input
    output = ""

    output << @builder.hidden_field("score", wrapper: false, label: false)

    template.content_tag :div, class: 'stars' do
      5.times do
        output << template.content_tag(:i, "", class: 'icon-star-empty star hover')
      end
    end

    output.html_safe
  end
end