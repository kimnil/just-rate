# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

jQuery ->
  $('.stars .star').hover (event) ->
    unless $(this).parent().hasClass('fixed')
      $(this).prevAll('.star').andSelf().removeClass('icon-star-empty').addClass('icon-star')
      $(this).nextAll('.star').removeClass('icon-star').addClass('icon-star-empty') 
  , ->
    unless $(this).parent().hasClass('fixed')
      $(this).siblings('.star').andSelf().removeClass('icon-star').addClass('icon-star-empty')
  
  $('.stars .star').click (event) ->
    $(this).prevAll('.star').andSelf().removeClass('icon-star-empty').addClass('icon-star')
    $(this).nextAll('.star').removeClass('icon-star').addClass('icon-star-empty')
    $(this).parent().addClass('fixed')
    $(this).siblings('input').val($(this).index())

  $('#department_id').selectize
    create: false
    onChange: (value) ->
      $('.course').show()
  
  $('#course_id').selectize
    theme: 'repositories'
    valueField: 'id'
    labelField: 'name'
    searchField: 'name'
    create: false
    preload: true
    option: (item, escape) ->
      return '<div>' +
          '<span class="title">' +
              '<span class="name">' + escape(item.name) + '</span>' +
              '<span class="by">' + escape(item.lecturer) + '</span>' +
          '</span>' +
          '<span class="description">' + escape(item.description) + '</span>' +
          '<ul class="meta">' +
              '<li class="course_number"><span>' + escape(item.course_number) + '</span></li>' +
          '</ul>' +
      '</div>'

    load: (query, callback) ->
      if !query.length
        return callback()

      $.ajax
        url: '/departments/' + $('#department_id').val() + '/course_list'
        dataType: 'json'
        method: 'GET'
        
        error: ->
          callback();
  
        success: (res) ->
          console.log(res)
          callback(res)
