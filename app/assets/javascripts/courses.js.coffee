# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

jQuery ->
  $('#course-search').selectize
    theme: 'repositories'
    valueField: 'id'
    labelField: 'name'
    searchField: 'name'
    create: false
    preload: true
    onChange: (value) ->
      window.location = "/courses/" + value
    option: (item, escape) ->
      return '<div>' +
          '<span class="title">' +
              '<span class="name">' + escape(item.name) + '</span>' +
              '<span class="by">' + escape(item.lecturer) + '</span>' +
          '</span>' +
          '<span class="description">' + escape(item.description) + '</span>' +
          '<ul class="meta">' +
              '<li class="course_number"><span>' + escape(item.course_number) + '</span></li>' +
          '</ul>' +
      '</div>'

    load: (query, callback) ->
      if !query.length
        return callback()

      $.ajax
        url: '/courses'
        dataType: 'json'
        method: 'GET'
        
        error: ->
          callback();
  
        success: (res) ->
          console.log(res)
          callback(res)