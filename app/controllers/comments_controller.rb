class CommentsController < ApplicationController

  # Make sure the user is authenticated
  before_filter :authenticate_user!, except: [:index, :show, :destroy, :update]
  before_filter :authenticate_admin!, only: [:destroy, :update]

  # GET /comments
  # GET /comments.json
  def index
    @course = Course.find(params[:course_id])
    @review = Review.find(params[:review_id])
    @comments = @review.comments.arrane(:order => :created_at)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @comments }
    end
  end

  # GET /comments/1
  # GET /comments/1.json
  def show
    @comment = Comment.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @comment }
    end
  end

  # GET /comments/new
  # GET /comments/new.json
  def new
    
    @comment = Comment.build

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @comment }
    end
  end

  def reply
    @comment = Comment.find(params[:comment_id])

    @reply = @comment.children.build

    respond_to do |format|
      format.html { render "reply_form" }
      format.json { render json: @comment }
      format.js { render partial: "reply", locals: { form: render_to_string("reply_form") } }
    end
  end

  def new_reply
    @comment = Comment.find(params[:comment_id])

    @reply = @comment.children.new(params[:comment])

    if @reply.save
      respond_to do |format|
        format.html { redirect_to course_review_path(@comment.review.course, @comment.review), notice: 'Comment was successfully created.' }
        format.json { render json: @comment }
      end
    end
  end

  # GET /comments/1/edit
  def edit
    @comment = Comment.find(params[:id])
  end

  # POST /comments
  # POST /comments.json
  def create
    @course = Course.find(params[:course_id])
    @review = @course.reviews.find(params[:review_id])
    @comment = @review.comments.build(params[:comment])

    respond_to do |format|
      if @comment.save
        format.html { redirect_to course_review_path(@course, @review), notice: 'Comment was successfully created.' }
        format.json { render json: @comment, status: :created, location: @comment }
      else
        format.html { render action: "new" }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /comments/1
  # PUT /comments/1.json
  def update
    @comment = Comment.find(params[:id])

    respond_to do |format|
      if @comment.update_attributes(params[:comment])
        format.html { redirect_to @comment, notice: 'Comment was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    @comment = Comment.find(params[:id])
    @comment.destroy

    respond_to do |format|
      format.html { redirect_to course_review_path(@comment.review.course, @comment.review), notice: 'Comment was successfully removed.' }
      format.json { head :no_content }
    end
  end
end
