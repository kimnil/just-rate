class StaticPageController < ApplicationController

  # Caching is off in dev env.
  caches_action :about_us
  caches_action :privacy
  caches_action :toc

  def about_us
  end

  def privacy
  end

  def toc
  end
end
