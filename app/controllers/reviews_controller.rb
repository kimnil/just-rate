class ReviewsController < ApplicationController

  # Make sure the user is authenticated
  before_filter :authenticate_user!, except: [:index, :show, :destroy]
  before_filter :authenticate_admin!, only: [:destroy, :update]

  # GET /reviews
  # GET /reviews.json
  def index
    @reviews = Review.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @reviews }
    end
  end

  # thumbs_up & thumbs_down for the reviews
  def vote_for
    @review = Review.find(params[:id])
    current_user.unvote_for(@review)
    current_user.vote_for(@review)
    respond_to do |format|
      format.html { redirect_to course_review_path(@review.course, @review) }
    end
  end

  def vote_against
    @review = Review.find(params[:id])
    current_user.unvote_for(@review)
    current_user.vote_against(@review)
    respond_to do |format|
      format.html { redirect_to course_review_path(@review.course, @review) }
    end
  end

  # Flag inappropriate for the reviews
  def flag 
    @review = Review.find(params[:id])

    @flag = @review.flags.new(params[:flag])
    @flag.flager = current_user

    respond_to do |format|
      if @flag.save
        format.html { redirect_to course_review_path(@review.course, @review), notice: 'Review has been flagged.' }
        format.json { render json: @flag, status: :created, location: @review}
      else
        format.html { redirect_to course_review_path(@review.course, @review), notice: 'You already flaged this.' }
        format.json { render json: @review.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /reviews/1
  # GET /reviews/1.json
  def show
    @course = Course.find(params[:course_id])
    @review = @course.reviews.find(params[:id])
    @comment = @review.comments.build

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @review }
    end
  end

  # GET /reviews/new
  # GET /reviews/new.json
  def new
    @course = Course.find(params[:course_id])
    @review = @course.reviews.build

    @course.rating_entities.each do |rating_entity|
      @rating = rating_entity.ratings.build
      @review.ratings << @rating
    end

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @review }
    end
  end

  def new_blank
    @review = Review.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @review }
    end
  end

  # GET /reviews/1/edit
  def edit
    @review = Review.find(params[:id])
  end

  # POST /reviews
  # POST /reviews.json
  def create
    @course = Course.find(params[:course_id])
    @review = @course.reviews.new(params[:review])
    @review.user = current_user

    respond_to do |format|
      if @review.save
        format.html { redirect_to course_review_path(@course, @review), notice: 'Review was successfully created.' }
        format.json { render json: @review, status: :created, location: @review }
      else
        format.html { render action: "new" }
        format.json { render json: @review.errors, status: :unprocessable_entity }
      end
    end
  end

  def create_blank
    @department = Department.find(params[:department_id])
    @course = @department.courses.find(params[:course_id])

    respond_to do |format|
      format.html { redirect_to new_course_review_path(@course, @review) }
      format.json { render json: @review, status: :created, location: @review }
    end
  end

  # PUT /reviews/1
  # PUT /reviews/1.json
  def update
    @review = Review.find(params[:id])

    respond_to do |format|
      if @review.update_attributes(params[:review])
        format.html { redirect_to @review, notice: 'Review was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @review.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reviews/1
  # DELETE /reviews/1.json
  def destroy
    @review = Review.find(params[:id])
    @review.destroy

    respond_to do |format|
      format.html { redirect_to course_path(@review.course), notice: 'Review was successfully removed.' }
      format.json { head :no_content }
    end
  end
end
