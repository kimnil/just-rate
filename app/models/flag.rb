class Flag < ActiveRecord::Base
  belongs_to :flagable, :polymorphic => true
  belongs_to :flager, polymorphic: true

  attr_accessible :description, :flagable, :flager, :type, :user_id
  validates_uniqueness_of :flagable_id, :scope => [:flagable_type, :flager_type, :flager_id]
end
