class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable, :omniauth_providers => [:facebook]

  # Setup accessible (or passwordrotected) attributes for your model
  attr_accessible :email, :name, :password, :password_confirmation,
                  :remember_me, :provider, :uid, :anonComments, :anonReviews

  validates :name, presence: true, on: :update

  has_many :flags, as: :flager
  has_many :flagged_reviews, through: :flags, source: :flagable, source_type: "Review"

  acts_as_voter

  def password_required?
    super if confirmed?
  end

  def anon_comments?
    anonComments
  end

  def anon_reviews?
    anonReviews
  end

  def password_match?
    self.errors[:password] << "can't be blank" if password.blank?
    self.errors[:password_confirmation] << "can't be blank" if password_confirmation.blank?
    self.errors[:password_confirmation] << "does not match password" if password != password_confirmation
    password == password_confirmation && !password.blank?
  end

  def to_s
    name || email
  end

  def self.find_for_facebook_oauth(auth, signed_in_resource=nil)
    data = auth.extra.raw_info
    if user = User.where(:email => data.email).first
      user
    else # Create a user with a stub password
      user = User.new(:name => data.name, :email => data.email, :password => Devise.friendly_token[0,20])
      user.skip_confirmation!
      user.save!
      user
    end
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end

end
