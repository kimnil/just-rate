class Rating < ActiveRecord::Base
  attr_accessible :rating_entity_id
  
  belongs_to :rating_entity
  belongs_to :review
  attr_accessible :score

  validates :score, presence: true
end
