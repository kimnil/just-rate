class RatingEntity < ActiveRecord::Base
  attr_accessible :description, :name

  has_and_belongs_to_many :courses
  has_many :ratings
end
