class Course < ActiveRecord::Base
  attr_accessible :description, :name, :course_number, :department_id, :rating_entities_attributes, :lecturer

  has_many :reviews, dependent: :destroy
  has_many :ratings, through: :reviews
  has_and_belongs_to_many :rating_entities

  belongs_to :department

  accepts_nested_attributes_for :rating_entities

  validates :description, :name, :course_number, :department_id, :lecturer, presence: true

  def self.sorted_by_score
    scoped.sort_by(&:average_score).reverse
  end

  def average_score
    if ratings.count > 0
      ratings.collect(&:score).sum / ratings.count
    else
      0
    end
  end
end
