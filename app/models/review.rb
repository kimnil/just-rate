class Review < ActiveRecord::Base
  attr_accessible :course_id, :text, :title, :ratings_attributes

  has_many :comments
  has_many :ratings

  accepts_nested_attributes_for :ratings
  
  belongs_to :course
  belongs_to :user

  acts_as_voteable
  
  has_many :flags, :as => :flagable
  
  validates :course_id, :text, :title, presence: true

  def self.sorted_by_created_at
    Review.all.sort_by(&:created_at).reverse
  end
end
