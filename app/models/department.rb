class Department < ActiveRecord::Base
  attr_accessible :name, :university_id

  has_many :courses
  belongs_to :university

  validates :name, presence: true

end
