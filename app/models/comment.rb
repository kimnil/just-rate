class Comment < ActiveRecord::Base
	has_ancestry

  attr_accessible :content, :user, :review_id, :parent_id

  belongs_to :review

  validates :content, presence: true

  def self.roots
    scoped.where(ancestry: nil)
  end
end
