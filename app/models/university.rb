class University < ActiveRecord::Base
  attr_accessible :city, :country, :name

  has_many :departments
  has_many :courses, through: :departments
end
