class AddAnonCommentsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :anonComments, :boolean, :default => false
  end
end
