class AddAnonReviewsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :anonReviews, :boolean, :default => false
  end
end
