class CreateRatingEntities < ActiveRecord::Migration
  def change
    create_table :rating_entities do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
