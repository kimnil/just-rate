class CreateFlags < ActiveRecord::Migration
  def change
    create_table :flags do |t|
      t.text :description
      t.references :flagable, polymorphic:true,:null => false
      t.references :flager,polymorphic:true
      t.string :type
      t.integer :user_id

      t.timestamps
    end
  end
end
