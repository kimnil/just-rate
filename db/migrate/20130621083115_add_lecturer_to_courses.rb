class AddLecturerToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :lecturer, :string
  end
end
