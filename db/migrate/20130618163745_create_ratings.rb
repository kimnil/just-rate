class CreateRatings < ActiveRecord::Migration
  def change
    create_table :ratings do |t|
      t.belongs_to :rating_entity
      t.belongs_to :review
      t.decimal :score

      t.timestamps
    end
    add_index :ratings, :rating_entity_id
    add_index :ratings, :review_id
  end
end
