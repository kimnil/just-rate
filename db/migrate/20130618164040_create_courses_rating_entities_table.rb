class CreateCoursesRatingEntitiesTable < ActiveRecord::Migration
  def up
    create_table :courses_rating_entities, id: false do |t|
      t.references :course
      t.references :rating_entity
    end

    add_index :courses_rating_entities, [:course_id, :rating_entity_id]
    add_index :courses_rating_entities, :rating_entity_id
  end

  def down
    drop_table :courses_rating_entities
  end
end
